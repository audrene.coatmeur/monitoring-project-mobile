if (typeof BigInt === 'undefined') global.BigInt = require('big-integer');

// App.js
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { View } from 'react-native';
import {reducer as form} from 'redux-form';

// see https://github.com/facebook/react-native/issues/14796
import { Buffer } from 'buffer';
global.Buffer = Buffer;

// see https://github.com/facebook/react-native/issues/16434
import { URL, URLSearchParams } from 'whatwg-url';
global.URL = URL;
global.URLSearchParams = URLSearchParams;

// see https://github.com/facebook/react-native/issues/12890
import RNEventSource from 'react-native-event-source';
global.EventSource = RNEventSource;

// Replace "book" with the name of resource type
import alerte from './app/reducers/alerte';
import component from './app/reducers/component';
import machine from './app/reducers/machine';
import site from './app/reducers/site';
import user from './app/reducers/user';
import Router from './app/Router';

export default class App extends Component {
  render() {
    const store = createStore(combineReducers({
      alerte,
      component,
      machine,
      site,
      user,
      form
    }), {}, applyMiddleware(thunk));
    return (
        <Provider store={store}>
          <View style={{flex: 1}}>
            <Router/>
          </View>
        </Provider>
    );
  }
}

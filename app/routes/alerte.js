import React from "react";
import { Scene, Actions } from "react-native-router-flux";
import List from "../components/alerte/List";
import Create from "../components/alerte/Create";
import Show from "../components/alerte/Show";
import Update from "../components/alerte/Update";
import { delayRefresh } from "../utils/helpers";

export default [
  <Scene
    rightTitle="Add"
    onRight={() => Actions.alerteCreate()}
    key="alerteList"
    component={List}
    title="List of Alertes"
    initial
  />,
  <Scene key="alerteCreate" component={Create} title="Add a new alerte" />,
  <Scene
    key="alerteShow"
    component={Show}
    title="Alerte"
    leftTitle="< List of Alertes"
    onLeft={() => {
      Actions.pop();
      delayRefresh();
    }}
  />,
  <Scene key="alerteUpdate" component={Update} title="Update Alerte" />,
];

import React from "react";
import { Scene, Actions } from "react-native-router-flux";
import List from "../components/component/List";
import Create from "../components/component/Create";
import Show from "../components/component/Show";
import Update from "../components/component/Update";
import { delayRefresh } from "../utils/helpers";

export default [
  <Scene
    rightTitle="Add"
    onRight={() => Actions.componentCreate()}
    key="componentList"
    component={List}
    title="List of Components"
    initial
  />,
  <Scene
    key="componentCreate"
    component={Create}
    title="Add a new component"
  />,
  <Scene
    key="componentShow"
    component={Show}
    title="Component"
    leftTitle="< List of Components"
    onLeft={() => {
      Actions.pop();
      delayRefresh();
    }}
  />,
  <Scene key="componentUpdate" component={Update} title="Update Component" />,
];

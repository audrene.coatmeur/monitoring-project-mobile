import React from "react";
import { Scene, Actions } from "react-native-router-flux";
import List from "../components/machine/List";
import Create from "../components/machine/Create";
import Show from "../components/machine/Show";
import Update from "../components/machine/Update";
import { delayRefresh } from "../utils/helpers";

export default [
  <Scene
    rightTitle="Add"
    onRight={() => Actions.machineCreate()}
    key="machineList"
    component={List}
    title="List of Machines"
    initial
  />,
  <Scene key="machineCreate" component={Create} title="Add a new machine" />,
  <Scene
    key="machineShow"
    component={Show}
    title="Machine"
    leftTitle="< List of Machines"
    onLeft={() => {
      Actions.pop();
      delayRefresh();
    }}
  />,
  <Scene key="machineUpdate" component={Update} title="Update Machine" />,
];

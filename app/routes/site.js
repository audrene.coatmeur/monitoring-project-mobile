import React from "react";
import { Scene, Actions } from "react-native-router-flux";
import List from "../components/site/List";
import Create from "../components/site/Create";
import Show from "../components/site/Show";
import Update from "../components/site/Update";
import { delayRefresh } from "../utils/helpers";

export default [
  <Scene
    rightTitle="Add"
    onRight={() => Actions.siteCreate()}
    key="siteList"
    component={List}
    title="List of Sites"
    initial
  />,
  <Scene key="siteCreate" component={Create} title="Add a new site" />,
  <Scene
    key="siteShow"
    component={Show}
    title="Site"
    leftTitle="< List of Sites"
    onLeft={() => {
      Actions.pop();
      delayRefresh();
    }}
  />,
  <Scene key="siteUpdate" component={Update} title="Update Site" />,
];

import React from "react";
import { Scene, Actions } from "react-native-router-flux";
import List from "../components/machine/List";
import Home from "../components/Home";

export default [
  <Scene key="mainhome" title="Main Home" initial>
    <Scene component={Home} key="home" title="Home"></Scene>
  </Scene>
];

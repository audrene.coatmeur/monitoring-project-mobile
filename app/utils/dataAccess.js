import { ENTRYPOINT } from "../config/entrypoint";
import { SubmissionError } from "redux-form";
import get from "lodash/get";
import has from "lodash/has";
import mapValues from "lodash/mapValues";

const MIME_TYPE = "application/ld+json";

export function fetch(id, options = {}) {
  if ("undefined" === typeof options.headers) options.headers = new Headers();
  if (null === options.headers.get("Accept"))
    options.headers.set("Accept", MIME_TYPE);

  if (
    "undefined" !== options.body &&
    !(options.body instanceof FormData) &&
    null === options.headers.get("Content-Type")
  )
    options.headers.set("Content-Type", MIME_TYPE);
    
  //ajout du token auth
  options.headers.set("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2Mzg5OTI5NDksImV4cCI6MTYzODk5NjU0OSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sImVtYWlsIjoidGVzdDJAbWFpbC5mciJ9.Cd__BPKIkplLU3USint9sD-t_uorXbMWp9RtXRfXsFS_7IsTvk1aq-WTgd5hA6g6QxTUFr2LwjM7Sev9IjUvyAXzqw-qgsKhAJolJnWTUO_e2ohYj3sUvp8pjCH1o_6Eqiw-5OVUr40jz7H1gcFDmN8fnXHZcHC0nyEtjEN2y5xa80CtZXt8xoge8SQ5mztsMMBuDdVjhohYl12BAGcHRgFLoFtTD_4Y3TNzVGWttVjBJ2HntEwyQhbZXUlOQI-7hzfzX4jnoirUpxQEr-x2wBMAlIvxL_83G7B3M-BCSVVlBzptNS9Cud0qzNWur6AOGD9217Y2cMEtJ4Q4Qk4r1hCDPQCHECLXvJbBhtssDGx1Wa77xDfZrPmrq2xUf5xP-x9EAGmF2XRZ2nCXWlvF2cdbyRKZfqFGpziDPolc-_uxTCxbQDu8656efk2-jGKcCGpigPtZnGiyX5NetIC1hr-iBHt_NGn0Te9DKMwzY0BmDf3r6ptT2deZaiuripA6EnNKVoVr5Ly0rys1935JjkhoIhFXlXnN1vgW5VUO7UiVIqSjfDsLPwtVxAsSLM-NTKl_zQ61XCo8aB17oLiT2DEknx-NLEanzeQFjXXDXxxipin2eD7RWc9pUaymjFOSs8ZYACOFPMtrclAY-Pinu0mX_6Q6U9y75mpeVvxHHAQ");
  
  return global.fetch(new URL(id, ENTRYPOINT), options).then((response) => {
    
    if (response.ok) return response;

    return response.json().then(
      (json) => {
        const error =
          json["hydra:description"] ||
          json["hydra:title"] ||
          "An error occurred.";
        if (!json.violations) throw Error(error);

        let errors = { _error: error };
        json.violations.forEach((violation) =>
          errors[violation.propertyPath]
            ? (errors[violation.propertyPath] +=
                "\n" + errors[violation.propertyPath])
            : (errors[violation.propertyPath] = violation.message)
        );

        throw new SubmissionError(errors);
      },
      () => {
        throw new Error(response.statusText || "An error occurred.");
      }
    );
  });
}

export function mercureSubscribe(url, topics) {
  topics.forEach((topic) =>
    url.searchParams.append("topic", new URL(topic, ENTRYPOINT))
  );

  return new EventSource(url.toString());
}

export function normalize(data) {
  if (has(data, "hydra:member")) {
    // Normalize items in collections
    data["hydra:member"] = data["hydra:member"].map((item) => normalize(item));

    return data;
  }

  // Flatten nested documents
  return mapValues(data, (value) =>
    Array.isArray(value)
      ? value.map((v) => get(v, "@id", v))
      : get(value, "@id", value)
  );
}

export function extractHubURL(response) {
  const linkHeader = response.headers.get("Link");
  if (!linkHeader) return null;

  const matches = linkHeader.match(
    /<([^>]+)>;\s+rel=(?:mercure|"[^"]*mercure[^"]*")/
  );

  return matches && matches[1] ? new URL(matches[1], ENTRYPOINT) : null;
}

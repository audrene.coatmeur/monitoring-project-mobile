import React from 'react';
import { Router, Scene, Stack } from 'react-native-router-flux';
// Replace "book" with the name of the resource type
import AlerteRoutes from './routes/alerte';
import ComponentRoutes from './routes/component';
import MachineRoutes from './routes/machine';
import SiteRoutes from './routes/site';
import UserRoutes from './routes/user';
import HomeRoute from './routes/home';
import Home from './components/Home';
import ListUsers from './components/user/List';
import ListSites from './components/site/List';
import ListMachines from './components/machine/List';
import ListComponents from './components/component/List';
import ListAlertes from './components/alerte/List';

const TabIcon = ({ selected, title }) => {
  return (
    <Text style={{color: selected ? 'red' :'white'}}>{title}</Text>
  );
}

const RouterComponent = () => {
  return (
      <Router>
        <Scene key="root">

          {/* Tab Container */}
          <Scene
            key="tabbar"
            title="Monitoring"
            tabs={true}
            tabBarStyle={{ backgroundColor: '#3a3a3a' }}
            activeTintColor="#b43c38"
            inactiveTintColor="white"
          >
            {/* Tab and it's scenes */}
            <Scene key="tabUsers" title="Users">
              <Scene key="Users"
                component={ListUsers}
                title="Users"
              />
            </Scene>

            {/* Tab and it's scenes */}
            <Scene key="tabSites" title="Sites">
              <Scene key="Sites"
                component={ListSites}
                title="Sites"
              />
            </Scene>

            {/* Tab and it's scenes */}
            <Scene key="tabMachines" title="Machines">
              <Scene key="Machines"
                component={ListMachines}
                title="Machines"
              />
            </Scene>

            {/* Tab and it's scenes */}
            <Scene key="tabComponents" title="Components">
              <Scene key="Components"
                component={ListComponents}
                title="Components"
              />
            </Scene>

            {/* Tab and it's scenes */}
            <Scene key="tabAlertes" title="Alertes">
              <Scene key="Alertes"
                component={ListAlertes}
                title="Alertes"
              />
            </Scene>

          </Scene>

        </Scene>
      </Router>
  );
};

const styles = {
  tab: {
    color: "white",
    fontSize: 10,
    backgroundColor: "#3a3a3a"
  },
}

export default RouterComponent;
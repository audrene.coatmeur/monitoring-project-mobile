import React, { Component } from "react";
import { Text, View } from "react-native";
import { Actions } from "react-native-router-flux";

class Home extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text
                    style={styles.welcome}
                    onPress={() => Actions.Users()}
                >
                    Home
                </Text>
            </View>
        )
    }
}

const styles = {
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#bb0000',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
      color: '#ffffff',
    },
};

export default Home;
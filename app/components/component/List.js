import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { ScrollView, View, Text } from "react-native";
import { ListItem } from "react-native-elements";
import { FlatList } from "react-native";
import { Actions } from "react-native-router-flux";
import Spinner from "../Spinner";
import { list, reset } from "../../actions/component/list";
import { success } from "../../actions/user/delete";
import { pagination } from "../../utils/helpers";

class ListComponent extends Component {
  componentDidMount() {
    this.props.reset();
    this.props.list();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.refresh !== this.props.refresh) {
      this.props.list();
    }
  }

  componentWillUnmount() {
    this.props.reset(this.props.eventSource);
  }

  static show(id) {
    Actions.componentShow({ id });
  }

  static renderRow(item) {
    item = item.item;
    const { listRowLeft, listRowRight, viewList } = styles;

    return (
      <ListItem
        bottomDivider={true}
        onPressRightIcon={() => ListComponent.show(item["id"])}
      >
        <ListItem.Content>
          <ListItem.Subtitle>
            <View>
              <View style={viewList}>
                <Text style={listRowLeft}>id: </Text>
                <Text style={[listRowRight, { fontWeight: "bold" }]}>
                  {item["@id"]}
                </Text>
              </View>
              <View style={viewList}>
                <Text style={listRowLeft}>state: </Text>
                <Text style={[listRowRight, { fontWeight: "bold" }]}>
                  {item["state"]}
                </Text>
              </View>
              <View style={viewList}>
                <Text style={listRowLeft}>name: </Text>
                <Text style={[listRowRight, { fontWeight: "bold" }]}>
                  {item["name"]}
                </Text>
              </View>
            </View>
          </ListItem.Subtitle>
        </ListItem.Content>
      </ListItem>
    );
  }

  render() {
    if (this.props.loading) return <Spinner size="large" />;

    if (this.props.error) {
      return (
        <View style={{ flex: 1 }}>
          <Text style={styles.textStyle}>{this.props.error}</Text>
        </View>
      );
    }

    if (this.props.retrieved) {
      return (
        <View style={{ flex: 1 }}>
          <FlatList
            data={this.props.retrieved["hydra:member"]}
            renderItem={ListComponent.renderRow}
            keyExtractor={item => item["@id"]} />
          {pagination(this.props.retrieved["hydra:view"], this.props.list)}
        </View>
      );
    }

    return <Spinner size="large" />
  }
}

const mapStateToProps = (state) => {
  const { retrieved, error, loading, eventSource } = state.component.list;
  return { retrieved, error, loading, eventSource };
};

const mapDispatchToProps = (dispatch) => ({
  list: (page) => dispatch(list(page)),
  reset: (eventSource) => dispatch(reset(eventSource)),
});

const styles = {
  viewList: {
    flex: 1,
    overflow: "hidden",
    flexDirection: "row",
  },
  listRowLeft: {
    flex: 1,
    borderBottomWidth: 1,
    padding: 5,
    justifyContent: "flex-start",
    position: "relative",
    color: "#b43c38",
    fontSize: 16,
  },
  listRowRight: {
    flex: 1,
    borderBottomWidth: 1,
    padding: 5,
    justifyContent: "flex-start",
    position: "relative",
    fontSize: 18,
  },
  textStyle: {
    textAlign: "center",
    marginTop: 20,
    fontSize: 18,
    color: "red",
    fontWeight: "bold",
  },
};

ListComponent.propTypes = {
  error: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  retrieved: PropTypes.object,
  eventSource: PropTypes.object,
  list: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  refresh: PropTypes.number,
};

export default connect(mapStateToProps, mapDispatchToProps)(ListComponent);

import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { TextInput, View, Text } from "react-native";
import {
  Input,
  Button,
} from "react-native-elements";

class Form extends Component {
  
  renderField(data) {
    const hasError = data.meta.touched && !!data.meta.error;
    data.input.value = data.input.value.toString();
    
    return (
      // <Input
      //   containerStyle={{ flexDirection: "row" }}
      //   errorMessage={data.meta.error}
      //   inputStyle={{ color: "black", flex: 1 }}
      //   label={data.input.name}
      //   labelStyle={{ color: "gray", fontSize: 20 }}
      //   onBlur={data.input.onBlur}
      //   onChange={data.input.onChange}
      //   onDragStart={data.input.onDragStart}
      //   onDrop={data.input.onDrop}
      //   onFocus={data.input.onFocus}
      //   placeholder={data.placeholder}
      //   multiline={true}
      //   value={data.input.value}
      // />
      <View>
        <Text>{data.input.name}</Text>
        <TextInput
          containerStyle={{ flexDirection: "row" }}
          inputStyle={{ color: "black", flex: 1 }}
          {...data.input}
          step={data.step}
          required={data.required}
          placeholder={data.placeholder}
          id={`site_${data.input.name}`}
          multiline={true}
        />
      </View>
    );
  }

  render() {
    const { handleSubmit, mySubmit } = this.props;

    return (
      <View style={{ backgroundColor: "white", paddingBottom: 20 }}>
        <Field
          component={this.renderField}
          name="Email"
          type="text"
          placeholder="email"
        />
        <Field
          component={this.renderField}
          name="Password"
          type="text"
          placeholder="password"
        />
        <Field
          component={this.renderField}
          name="Name"
          type="text"
          placeholder="name"
        />
        <Button
          buttonStyle={styles.button}
          title="SAVE"
          onPress={handleSubmit(mySubmit)}
        />
      </View>
    );
  }
}

const styles = {
  button: {
    flex: 1,
    backgroundColor: "#3faab4",
    borderRadius: 5,
    borderWidth: 0,
    borderColor: "transparent",
    width: 300,
    height: 50,
    margin: 20,
  },
  placeholderStyle: {
    paddingRight: 10,
  },
};

export default reduxForm({
  form: "user",
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
})(Form);

module.exports = {
    transformer: {
      getTransformOptions: async () => ({
        transform: {
          experimentalImportSupport: false,
          inlineRequires: false,
        },
      }),
    },
    resolver: {
      sourceExts: ['jsx', 'js', 'ts', 'tsx', 'native', 'android.ts',' native.ts', 'ts', 'android.tsx', 'native.tsx', 'tsx', 'android.js', 'native.js', 'js', 'android.jsx', 'native.jsx', 'jsx', 'android.json', 'native.json', 'json'], //add here
    },
};